from CustomHTMLParser import HTMLParser
from HtmlHelper import HtmlHelper

# For further processing of html for Date Range Fields that cannot be done by ReskinBaseHtmlParser. 
# Assumes ReskinBaseHtmlParser has already been run, and we are just dealing with the base code (<td>, <tr>, etc tags removed)
# IMPORTANT: for now, you need to add the last closing </li> manually after running

helper = HtmlHelper()


class DateRangeHtmlParser(HTMLParser):

	def handle_startendtag(self, tag, attrs, raw):
		self.handle_starttag(tag, attrs, raw)

	def handle_starttag(self, tag, attrs, raw):

		if tag == "input":
			if self.__firstInput == 1:
				self.html += helper.constructStartTag("label", [("for","input_START_DATE")]) + "Date Range" + "</label>\n"
				self.html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
				self.html += helper.constructDivStartTag(["datepicker__prefix"]) + "from" + "</div>\n"
				attrs = [("class", "js-datepicker")]
				self.html += helper.addAttrsToTag(attrs, "input", raw) + "\n"
				self.html += "</div>" + "\n"
				self.__firstInput =0
			else:
				self.html += helper.constructStartTag("label", [("for","input_END_DATE"), ("class", "visuallyhidden")]) + "End Date" + "</label>\n"
				self.html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
				self.html += helper.constructDivStartTag(["datepicker__prefix"]) + "to" + "</div>\n"
				attrs = [("class", "js-datepicker")]
				self.html += helper.addAttrsToTag(attrs, "input", raw) + "\n"
				self.html += helper.constructEndTag("div") + "\n"
		else:
			self.html += raw


	def handle_endtag(self, tag, raw):
		self.html += raw

	def handle_data(self, data):
		if "(mm/dd/yyyy)" not in data:
			self.html += data

	def handle_terastarttag(self,tag,attrs, raw):
		self.html += raw 		

	def handle_teraendtag(self,tag, raw):
		self.html += raw

	def __init__(self):
		HTMLParser.__init__(self)
		self.__firstInput = 1 
		self.html = ""
