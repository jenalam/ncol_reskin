class HtmlHelper():

	# Constructs a start tag with list of attributes
	# tag: the name of the html tag (eg. "li" or "input")
	# attrs: a list of tuples containing tag attributes
	#	eg. <label for="some_id" class="some_class"> 
	#		tag = "label"
	#		attrs = [ 
	#					("for", "some_id"), 
	#					("class","some_class")
	#				]
	def constructStartTag(self, tag, attrs):
		html = "<" + tag
		for attr in attrs: 
			key,val = attr
			if key != None and val != None:
				html+= " " + key + "=\"" + val + "\""
		html+=">"
		return html

	def constructDivStartTag(self, extra_classes):
		attrs = []
		class_string = ""
		for extra_class in extra_classes:
			class_string += extra_class + " "
		attrs = [("class", class_string)]

		return self.constructStartTag("div", attrs)

	def constructFieldsetStartTag(self,extra_classes):
		attrs = []
		if not extra_classes:
			attrs = [("class", "field")]
		else:
			class_string = "field"
			for extra_class in extra_classes:
				class_string += " " + extra_class 
			attrs = [("class", class_string)]

		return self.constructStartTag("fieldset", attrs)

	def wrapInFieldsetDiv(self, extra_classes, baseCode):
		return self.constructFieldsetStartTag(extra_classes) + baseCode + self.constructEndTag("fieldset")

	def constructLabelStartTag(self, for_id, label):
		html = "<label for=\"" + for_id + "\">" + label
		return html

	def constructLabel(self, for_id, label):
		html = "<label for=\"" + for_id + "\">" + label + "</label>"
		return html

	def constructSelfClosingTag(self, tag, attrs):
		html = "<" + tag
		for attr in attrs: 
			key,val = attr
			if key != None and val != None:
				html+= " " + key + "= \"" + val + "\""
		html+=" />"
		return html

	def constructEndTag(self, tag):
		return "</" + tag + ">"

	def containsTitle(self, attrs):
		for attr in attrs:
			key,val = attr
			if key=='class' and val == 'fieldDesc':
				return 1
		return 0

	def addAttrsToTag(self, attrs, tag, baseCode):
		newBaseCode = baseCode.split("<" + tag,1)
		returnStr = "<" + tag + " "
		for attr in attrs:
			key, val = attr
			returnStr += key + "= \"" + val + "\" "
		return newBaseCode[0] + returnStr + newBaseCode[1]