# README #

##Introduction:##
This script can reskin: 


 1. Various Form fields and  **NOTE: reworking this function, it's currently broken**
 2. Adding <strong></strong> tags in error messages in the Validate part of the taf

## Files ##
* reskin.py - the file to run when reskinning
* old.html - the file to paste code to reskin
* new.html - the output file which will contain the reskinned content after running script
* other files... - contains the good stuff that makes the script work. Ask Jen if you want to understand it

## How do I get set up? ##

* Clone the git repo on your machine
* Ensure you have a bash shell that can run Python 2

## How to use ##
First, change to the directory of the cloned repo
```
#!python

cd <PATH_TO_REPO>
```


To reskin for form fields, this is the general command to run: 
```
#!python

./reskin.py -f [FIELD_TYPE]
```
### Possible FIELD_TYPE options are: ### 
* ['' or 't'] Text Field Input 
* ['d' or 'date'] Date Field Input 
* ['p' or 'phone'] Phone Field input (text field with phone icon)
* ['pr' or 'price'] Price Field Input (text field with $ prefix) 
* ['e' or 'email'] Email Field Input (text field with email icon)
* ['n' or 'numeric' or 'qty'] Quantity Field Input (text field with # prefix) 
* ['r' or 'radio'] Radio Select Fields
* ['s' or 'select'] Dropdown Select Fields
* ['ta' or 'textarea'] Text Areas with tiny MCE

To reskin for adding <strong></strong> tags to error message validation, run: 
```
#!python

./reskin.py -v 
```

## Example: Reskinning a Text Field ##
I want to reskin one of the text field inputs of a taf. 

First, I'll copy the table row that contains the field into **old.html** in the Reskin folder. 

**IMPORTANT: only one input field at a time! You can't couple multiple input fields to reskin at the same time! **   

```
#!html
<tr>
	<td class="fieldDesc">Title <span class="req">*</span></td>
	<td><input name="SBTEVNT_TITLE" type="text" size="40" maxlength="50" /></td>
</tr>

```

Then I'll run 
```
#!python

./reskin.py -f t
```

Then I'll go to **new.html**, and TADA! 
```
#!html
<fieldset class="field">
	<label for="input_TITLE">Title
		<span class="req">*</span>
	</label>
	<div class="field__input-wrapper">
		<input id= "input_TITLE" class= "input-medium" name= "SBTEVNT_TITLE" type= "text" size= "40" maxlength= "50" />
	</div>
</fieldset>

```

I can then copy paste this result into the taf, replacing the old text. **Be sure to do a quick double check, particularly where terascript is involved, as the script is not perfect!**

## Example: Reskinning for adding <strong></strong> tags to error message validation ##
**NOTE: reworking this function, it's currently broken**
I want to reskin this validation form so that there are <strong></strong> tags around the items for the errorMessage.

First, I'll copy it into **old.html**

```
#!html


```

Then, I'll run:
```
#!bash

./reskin.py -v 
```

And tada! This should appear in *new.html* for me to copy paste back into the taf