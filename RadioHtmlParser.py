from CustomHTMLParser import HTMLParser
from HtmlHelper import HtmlHelper

# For further processing of html for radio buttons that cannot be done by ReskinBaseHtmlParser. 
# Assumes ReskinBaseHtmlParser has already been run, and we are just dealing with the base code (<td>, <tr>, etc tags removed)
# IMPORTANT: for now, you need to add the last closing </li> manually after running

helper = HtmlHelper()


class RadioHtmlParser(HTMLParser):

	def handle_startendtag(self, tag, attrs, raw):
		self.handle_starttag(tag, attrs, raw)
		
	def handle_starttag(self, tag, attrs, raw):

		if tag == "input":
			if self.__firstInput == 0: 
				self.html += "</li>\n"
			else:
				self.__firstInput = 0
			self.html += "<li class=\"field--inline-radios__item\">\n"
		elif tag == "label":
			self.__alreadyHasLabels = 1 
		
		self.html += raw


	def handle_endtag(self, tag, raw):
		self.html += raw

	def handle_data(self, data):
		self.html += data

	def handle_terastarttag(self,tag,attrs, raw):
		self.html += raw 		

	def handle_teraendtag(self,tag, raw):
		self.html += raw

	def __init__(self):
		HTMLParser.__init__(self)
		self.__firstInput = 1
		self.__alreadyHasLabels = 0	# This is currently unused 
		self.html = ""
