#! /usr/bin/python

# Base class for all reskin parsers. It will find the label title and perform basic tasks shared by all parsers

import re
from HtmlHelper import HtmlHelper
from CustomHTMLParser import HTMLParser

helper = HtmlHelper()

removeTags = ["table", "td", "tr", "span", "em","br"]
class ReskinBaseHtmlParser(HTMLParser):

	def handle_startendtag(self, tag, attrs, raw):
		self.handle_starttag(tag, attrs, raw)

	def handle_starttag(self, tag, attrs, raw):
		if tag == "td":
			self.__isLabel = helper.containsTitle(attrs)
		elif tag == "span": 
			self.__isSpan = 1
 		
		if tag not in removeTags:
			self.baseCode += raw

	def handle_endtag(self, tag, raw):
		if tag == "span":
			self.__isSpan = 0 

		if tag not in removeTags:
			self.baseCode += raw

	def handle_terastarttag(self, tag, attrs, raw):
		if tag not in removeTags:
			self.baseCode += raw

	def handle_teraendtag(self,tag, raw):
		if tag not in removeTags:
			self.baseCode += raw
		
	def handle_data(self, data):
		if self.__isLabel == 1:
			self.label = data.strip()
			self.id="input_" + re.sub(r'[^A-Za-z0-9_]','',self.label.upper().replace(" ", "_"))
			self.__isLabel=0
			print "ID:\t" + self.id
		elif self.__isSpan == 1:
			if "*" in data: 
				self.label = self.label + "<span class=\"req\">*</span>"
			else:
				self.baseCode = "<span>" + data + "</span>" 
		else:
			self.baseCode+=data


	def __init__(self):
		HTMLParser.__init__(self)
		self.__isLabel = 0
		self.__isSpan = 0 
		self.label=""
		self.id=""
		self.baseCode=""
		self.addIdToTag="input" #input by default