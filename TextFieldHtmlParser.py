#! /usr/bin/python
import re
from HtmlHelper import HtmlHelper
from CustomHTMLParser import HTMLParser

helper = HtmlHelper()
regex = re.compile('[^a-zA-Z0-9]_')
class TextFieldHtmlParser(HTMLParser):

	def handle_starttag(self, tag, attrs):
		if tag == "td":
			self.isTitle = helper.containsTitle(attrs)
		elif tag == "span": 
			self.isRequired = 1
		elif tag != "tr":	
			if tag == "input": 
				self.html += indent(self.numIndent)
				self.html+= "<div class=\"field__input-wrapper\">\n"
			
			self.html += indent(self.numIndent)
			
			if tag == "input":
				extraAttrs = [("id",self.label),("class", "input-medium")];
				attrs = extraAttrs + attrs
				self.html += helper.constructSelfClosingTag(tag, attrs) + "\n"
			else:
				self.html += helper.constructStartTag(tag, attrs) + "\n"

			self.numIndent+=1

	def handle_endtag(self, tag):
		if tag != "td" and tag != "tr" and tag != "span" and tag !="input":
			self.numIndent-=1
			self.html += indent(self.numIndent)
			self.html+= helper.constructEndTag(tag) + "\n"
		elif tag == "input": 
			self.html += indent(self.numIndent)
			self.html+="</div>\n"

	def handle_terastarttag(self,tag,attrs):
		self.html += tag 		#Currently tag contains entire terascript content within <@ and >, attrs is empty 

	def handle_teraendtag(self,tag):
		self.html += "</@" + tag.upper() + ">"
		
	def handle_data(self, data):
		if self.isTitle == 1:
			self.title = data.strip()
			self.label="input_" + regex.sub('',self.title.upper().replace(" ", "_"))
			self.isTitle=0
		elif data != "*":
			self.html+=data + "\n"

	def __init__(self):
		HTMLParser.__init__(self)
		self.isTitle = 0
		self.title=""
		self.label=""
		self.html=""
		self.numIndent = 2
		self.isRequired = 0 

def indent(numIndent):
	text = ""
	for i in range(0,numIndent): 
		text +="\t"
	return text
