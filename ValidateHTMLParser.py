from CustomHTMLParser import HTMLParser
from HtmlHelper import HtmlHelper

import re

findverbs = re.compile(r'\s*( is | cannot )\s*')
findErrorMsg = re.compile(r'<li>(.*?)</li>')
helper = HtmlHelper()

class ValidateHTMLParser(HTMLParser):

	def handle_starttag(self, tag, attrs, raw):
		i=1

	def handle_terastarttag(self,tag,attrs, raw):
		if tag == "ASSIGN" and "<@VAR local$l_errorMessage>" in raw:
			errorMsgOld=""
			errorMsgNew=""
			errorMsgMatch = findErrorMsg.search(raw)
			if errorMsgMatch:
				errorMsgOld = errorMsgMatch.group(1)
				m = findverbs.search(errorMsgOld)
				if m:
					before, after = errorMsgOld.split(m.group(0))
					errorMsgNew = "<strong>" + before + "</strong>"  + m.group(0)  + after 
				else:
					errorMsgNew = "<strong>" + errorMsgOld.split(' ', 1)[0] + "</strong>" + errorMsgOld.split(' ', 1)[1] 
			
			raw = raw.replace(errorMsgOld, errorMsgNew)
			
		self.html += raw

	def handle_teraendtag(self,tag, raw):
		self.html += raw

	def handle_endtag(self, tag, raw):
		self.html += raw

	def handle_data(self, data):
		self.html += data

	def __init__(self):
		HTMLParser.__init__(self)
		self.html = ""

