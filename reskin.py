#! /usr/bin/python
import sys
import re

from RadioHtmlParser import RadioHtmlParser
from TextFieldHtmlParser import TextFieldHtmlParser
from ValidateHTMLParser import ValidateHTMLParser
from DateHtmlParser import DateHtmlParser
from HtmlHelper import HtmlHelper
from ReskinBaseHtmlParser import ReskinBaseHtmlParser
from DateRangeHtmlParser import DateRangeHtmlParser

helper = HtmlHelper()

fieldTypeOptions = [
("t, textfield", "Reskin Text Field Input"),
("d, date", "\tReskin Date Field Input"),
("p, phone", "Reskin Phone Field input (text field with phone icon)"),
("pr, price", "Reskin Price Field Input (text field with $ prefix)"),
("e, email", "Reskin Email Field Input (text field with email icon)"),
("n, numeric, qty", "Reskin Quantity Field Input (text field with # prefix)"),
("r, radio", "Reskin Radio Select Fields"),
("s, select", "Reskin Dropdown Select Fields"),
("ta, textarea", "Reskin Text Area"),
("c, checkbox", "Reskin Inline Checkbox")
]

def printFieldTypeOptions():
	options_string = "\tField Type Options: \n"
	for option in fieldTypeOptions:
		key, val = option
		options_string += "\t\t" + key + "\t\t" + val
		options_string += "\n"
	print options_string

def reskinTextField():
	print("Reskinning text field...\n")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = []
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructLabel(parser.id, parser.label) + '\n'

	# add field input wrapper
	html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
	
	baseCode = parser.baseCode.strip() + "\n"
	attrs = [("id", parser.id), ("class", "input-medium")]
	html += helper.addAttrsToTag(attrs, "input", baseCode)
		
	html += helper.constructEndTag("div") + '\n'

	# add </fieldset> tags around the html
	html += helper.constructEndTag("fieldset")

	return html

def reskinInlineCheckbox():
	print("Reskinning inline check box...")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--inline-check"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"
	
	baseCode = parser.baseCode.strip() + "\n"
	html += baseCode
	# add </fieldset> tags around the html
	html += "</fieldset>" 

	return html

def reskinTextArea():
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--tinymce"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructLabel(parser.id, parser.label) + '\n'

	# add field input wrapper
	html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
	
	baseCode = parser.baseCode.strip() + "\n"
	html += baseCode
		
	html += helper.constructEndTag("div") + '\n'

	# add </fieldset> tags around the html
	html += helper.constructEndTag("fieldset")

	return html

def reskinPhoneField():
	print("Reskinning phone field...\n")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--input-icon", "field--input-icon--phone"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructLabel(parser.id, parser.label) + '\n'

	# add field input wrapper
	html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
	
	baseCode = parser.baseCode.strip() + "\n"
	attrs = [("id", parser.id), ("class", "input-medium")]
	html += helper.addAttrsToTag(attrs, "input", baseCode)
		
	html += helper.constructEndTag("div") + '\n'

	# add </fieldset> tags around the html
	html += helper.constructEndTag("fieldset")

	return html

def reskinDateField():
	print("Reskinning date field...")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--input-icon", "field--input-icon--date", "field--datepicker"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructLabel(parser.id, parser.label) + '\n'

	# add field input wrapper
	html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
	
	baseCode = parser.baseCode.strip() + "\n"
	attrs = [("id", parser.id), ("class", "js-datepicker")]
	html += helper.addAttrsToTag(attrs, "input", baseCode)
		
	html += helper.constructEndTag("div") + '\n'

	# add </fieldset> tags around the html
	html += helper.constructEndTag("fieldset")

	return html

def reskinDateRangeField():
	print("Reskinning date range field...")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--input-icon", "field--input-icon--date", "field--datepicker-range"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	baseCode = parser.baseCode.strip() + "\n"
	dateRangeParser = DateRangeHtmlParser()
	dateRangeParser.feed(newHtml)

	html += dateRangeParser.html 
		

	# add </fieldset> tags around the html
	html += helper.constructEndTag("fieldset")

	return html

def reskinSubmit():
	print("Reskinning submit field...\n")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--submit"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# add field input wrapper
	html += helper.constructDivStartTag(["field--submit__submit"]) + '\n'
	
	html += parser.baseCode.strip() + "\n"
		
	html += '</div>\n'

	# add </fieldset> tags around the html
	html += "</fieldset>"

	return html

def reskinNumericTextField():
	print("Reskinning numeric field...")
	parser = TextFieldHtmlParser()
	parser.feed(file.read())
	html = "<fieldset class=\"field field--qty\">\n"
	html += "\t<label for=\"" + parser.label + "\">" + parser.title
	if parser.isRequired == 1: 
		html += "<span class=\"req\">*</span>"
	html += "</label>\n" + parser.html.strip() + "\n</fieldset>\n"
	return html

def reskinPriceField():
	print("Reskinning price field...")
	parser = TextFieldHtmlParser()
	parser.feed(file.read())
	html = "<fieldset class=\"field field--amount\">\n"
	html += "\t<label for=\"" + parser.label + "\">" + parser.title
	if parser.isRequired == 1: 
		html += "<span class=\"req\">*</span>"
	html += "</label>\n" + parser.html.strip().replace('$','') + "\n</fieldset>\n"
	return html

def reskinEmailField():
	print("Reskinning email field...")
	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--input-icon", "field--input-icon--email"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructLabel(parser.id, parser.label) + '\n'

	# add field input wrapper
	html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n'
	
	baseCode = parser.baseCode.strip() + "\n"
	attrs = [("id", parser.id), ("class", "input-medium")]
	html += helper.addAttrsToTag(attrs, "input", baseCode)
		
	html += helper.constructEndTag("div") + '\n'

	# add </fieldset> tags around the html
	html += helper.constructEndTag("fieldset")
	return html

def reskinSelectField():
	print("Reskinning select field...")

	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--select"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructLabel(parser.id, parser.label) + '\n'

	html += helper.constructDivStartTag(["field__input-wrapper"]) + '\n' # <div class="field__input-wrapper">
	html += helper.constructDivStartTag(["custom-select", "custom-select--inline"]) + "\n" # <div class="custom-select custom-select--inline">

	baseCode = parser.baseCode.strip() + "\n"
	attrs = [("id", parser.id)]
	html += helper.addAttrsToTag(attrs, "select", baseCode)

	html += helper.constructEndTag("div") + '\n'	# </div>
	html += helper.constructEndTag("div") + '\n'	# </div>

	html += helper.constructEndTag("fieldset")	#</fieldset>

	return html

def reskinRadioField():
	print("Reskinning radio field...")

	html = ""
	parser = ReskinBaseHtmlParser()
	parser.feed(file.read())

	# add <fieldset class="..."> to html
	extra_classes = ["field--inline-radios"]
	html += helper.constructFieldsetStartTag(extra_classes) + "\n"

	# reskin and add the label
	html += helper.constructStartTag("p", [("class", "field--inline-radios__desc")])
	html += parser.label + helper.constructEndTag("p") + '\n'	#<p class="filed--inline-radios__desc"> label </p>

	html += helper.constructStartTag("ul", [("class", "field--inline-radios__items")]) + "\n" # <ul class="field--inline-radios__items">

	baseCode = parser.baseCode.strip() + "\n"
	radioParser = RadioHtmlParser()
	radioParser.feed(baseCode)
	html += radioParser.html.strip() + "\n" + "</li>\n"		#For now have to manually add </li> because can't figure out good way for parser to detect end 

	html += helper.constructEndTag("ul") + '\n'	# </ul>

	html += helper.constructEndTag("fieldset")	#</fieldset>

	return html

	parser = RadioHtmlParser()
	parser.feed(file.read())
	html = "<fieldset class=\"field field--inline-radios\">\n"
	html += "\t<p class=\"field--inline-radios__desc\">" + parser.title
	if parser.isRequired == 1: 
		html += "<span class=\"req\">*</span>"
	html += "</p>\n" + "\t\t<ul class=\"field--inline-radios__items\">\n" + parser.html.strip() + "\n\t\t</ul>\n" "\n</fieldset>\n"
	return html


# instantiate the parser and fed it some HTML
file = open("old.html", "r")
outfile = open("new.html", "w")

if(len(sys.argv) < 2):
	print("Please enter an argument: \n\t [ -v ] for adding <strong> tags to Validate form \n\t [ -f ] for form validation")
elif(sys.argv[1] == "-v"):
	print("Reskinning for Validation...")
	parser = ValidateHTMLParser()
	parser.feed(file.read())
	# print(parser.html)
	outfile.write(parser.html)
elif(sys.argv[1] == "-copy"):	# For quick access to common code for copy pasting
	html =""
	if len(sys.argv) == 3:
		if sys.argv[2] == "l" or sys.argv[2] == "layout":
			file = open("CodeToCopy/basic_layout.html")
			html = file.read()
		elif sys.argv[2] == "t" or sys.argv[2] == "table":
			file = open("CodeToCopy/table.html")
			html = file.read()
		elif sys.argv[2] == "e" or sys.argv[2] == "error" or sys.argv[2] == "success":
			file = open("CodeToCopy/error_success.html")
			html = file.read()
		elif sys.argv[2] == "v" or sys.argv[2] == "vars": 
			file = open("CodeToCopy/vars.html")
			html = file.read()
		elif sys.argv[2] == "r" or sys.argv[2] == "resumeable":
			file = open("CodeToCopy/resumeable_upload.html")
			html = file.read()
		print(html)
		outfile.write(html)
elif(sys.argv[1] == "-f"):
	html = ""
	print("Reskinning for Forms...")
	if len(sys.argv) == 3:
		if sys.argv[2] == "r" or sys.argv[1] == "radio":
			html = reskinRadioField()
		elif sys.argv[2] == "d" or sys.argv[1] == "date":
			html = reskinDateField()
		elif sys.argv[2] == "dr" or sys.argv[1] == "daterange":
			html = reskinDateRangeField()
		elif sys.argv[2] == "p" or sys.argv[2] == "phone": 
			html = reskinPhoneField()
		elif sys.argv[2] == "pr" or sys.argv[2] == "price":
			html = reskinPriceField()
		elif sys.argv[2] == "e" or sys.argv[2] == "email": 
			html = reskinEmailField()
		elif sys.argv[2] =="ta" or sys.argv[2] == "textarea":
			html = reskinTextArea()
		elif sys.argv[2] == "n" or sys.argv[2] == "numeric" or sys.argv[2] == "qty": 
			html = reskinNumericTextField()
		elif sys.argv[2] == "s" or sys.argv[2] == "select":
			html = reskinSelectField()
		elif sys.argv[2] == "su" or sys.argv[2] == "submit": 
			html = reskinSubmit()
		elif sys.argv[2] == "t" or sys.argv[2] == "textfield":
			html = reskinTextField()
		elif sys.argv[2] == "c" or sys.argv[2] == "checkbox":
			html = reskinInlineCheckbox()
		elif sys.argv[2] == "x":
			parser = ReskinBaseHtmlParser()
			parser.feed(file.read())
			html = parser.baseCode
		else: 
			print "\n\tOops! Expecting a field type option after -v (eg. ./reskin.py -v t)"
			printFieldTypeOptions()
			html=""
	else:
		print "\n\tOops! Expecting a field type option after -v: (eg. ./reskin.py -v t)"
		printFieldTypeOptions()
		html = ""
	outfile.write(html)
else:
	print("I don't recognize that argument!")
